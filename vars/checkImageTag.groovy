def call(checkBranch, defaultTag, img = "nrp"){
// This function returns the image tag to be used by pipeline
// 
// In case the tag, corresponding to checkBranch exists in the registry, this tag is pulled and returned
//
// Otherwise the defaultTag is pulled and returned
    try
    {
        def checkTag = checkBranch.replace("/", "-");
        docker.withRegistry("https://${env.NEXUS_REGISTRY_IP}:${env.NEXUS_REGISTRY_PORT}", 'nexusadmin') {
            image = docker.image("${img}:${checkTag}")
            image.pull()
        }
        return checkTag;
    }
    catch (exc)
    {
        docker.withRegistry("https://${env.NEXUS_REGISTRY_IP}:${env.NEXUS_REGISTRY_PORT}", 'nexusadmin') {
            image = docker.image("${img}:${defaultTag}")
            image.pull()
        }
        return defaultTag;
    }
}