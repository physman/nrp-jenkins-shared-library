def call(folder, repoUrl, topicBranch, defaultBranch, user) {
// cloneRepoTopic: 
//      1 - directory to checkout
//      2 - repo
//      3 - name of topic branch
//      4 - default branch if topic unavailable
//      5 - username for chown
    def exists = fileExists folder
    if (exists) {
        dir(folder) {
            sh 'git fetch && git reset --hard HEAD'
        }
    }
    else {
        dir(folder) {
            try {
                echo "${folder}: Trying to checkout branch ${topicBranch}."
                git branch: topicBranch, url: repoUrl
            }
            catch (e) {
                echo "${folder}: Branch ${topicBranch} is not available, getting ${defaultBranch} instead."
                git branch: defaultBranch, url: repoUrl
            }
            sh "chown -R ${user} ./"
        }
    }
}