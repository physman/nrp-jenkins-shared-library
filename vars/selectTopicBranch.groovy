def call(branch_name, change_branch){
// This function is used to choose the correct branch name as topic
// 
// In case there is a PR for a branch, then Jenkins runs a pipeline for this pull request, not for the branch, 
// even if there are new commits to the branch (until PR is not closed). The BRANCH_NAME variable in this case is something like PR-###
// The name of the branch which is merged is stored in CHANGE_BRANCH variable. Thus, we should choose CHANGE_BRANCH as topic
//
// If there is a branch without PR, then Jenkins creates build for it normally for every push and the branch name is stored in BRANCH_NAME variable.
// CHANGE_BRANCH is empty in this case. Thus, we choose BRANCH_NAME as topic for branches without PR.
    if (change_branch){
        return change_branch
    }
    else {
        return branch_name
    }
}